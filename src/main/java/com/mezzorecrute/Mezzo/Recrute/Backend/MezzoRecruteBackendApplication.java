package com.mezzorecrute.Mezzo.Recrute.Backend;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.ERole;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.Role;
import com.mezzorecrute.Mezzo.Recrute.Backend.role.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MezzoRecruteBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MezzoRecruteBackendApplication.class, args);
	}


	@Bean
	public CommandLineRunner commandLineRunner(
			RoleRepository repository
	){
		return args -> {

//            create deferant roles dans la base -->

//            1-->ROLE for Employe
		if (repository.findAll().isEmpty()) {
			Role role =  Role.builder()
					.name(ERole.ROLE_EMPLOYEE).build();
			repository.save(role);

//            2-->ROLE for ADMIN
			Role role1 =  Role.builder()
					.name(ERole.ROLE_ADMIN).build();
			repository.save(role1);

//            3-->ROLE for Responsable de recrutement
			Role role2 =  Role.builder()
					.name(ERole.ROLE_RC).build();
			repository.save(role2);

//            4-->ROLE for Recrute
			Role role3 =  Role.builder()
					.name(ERole.ROLE_RECRUTEUR).build();
			repository.save(role3);

//            4-->ROLE for Responsable de departemnt
			Role role4 =  Role.builder()
					.name(ERole.ROLE_RD).build();

//            4-->ROLE for Candidat
			repository.save(role4);
			Role role5 =  Role.builder()
					.name(ERole.ROLE_CANDIDAT).build();
			repository.save(role5);
		} else {

		}

	};
}
}



