package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins =  "http://localhost:4200", maxAge = 3600, allowCredentials="true")
@RequestMapping("/api/test/utilisateurs")
@RequiredArgsConstructor
public class UtilisateurController {
    private final UtilisateurService service;

// methode that call for get all users
    @GetMapping("/getall")
    public List<Utilisateur> getAllusers(){
        return service.getAllUsers();
    }

    // methode that call for get all users
    @PostMapping("/save")
    public ResponseEntity<String> save (@RequestBody Utilisateur utilisateur){
        return ResponseEntity.ok(service.save(utilisateur));
    }

    // methode that call for get one user by his id
    @GetMapping(path ="/getuser/{id}")
    public Utilisateur findUserById(@PathVariable String id) {
        return service.finduserById(id);
    }
// methode for update user with put mapping it need the id when you gonna update something
    @PutMapping(path ="/update/{id}")
    public Utilisateur updateUser( @RequestBody Utilisateur utilisateur) {
        return service.updateUser(utilisateur);
    }
// methode that add new user
    @PostMapping(path="/adduser")
    public Utilisateur createUser( @RequestBody Utilisateur utilisateur) {
        return service.addUser(utilisateur);
    }
    @DeleteMapping(path="/{id}")
    public void deleteuser(@PathVariable String id){
        service.deleteUser(id);
    }

}
