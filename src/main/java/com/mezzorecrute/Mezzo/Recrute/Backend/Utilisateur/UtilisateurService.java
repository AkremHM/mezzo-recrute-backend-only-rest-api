package com.mezzorecrute.Mezzo.Recrute.Backend.Utilisateur;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UtilisateurService {
    @Autowired
    private UtilisateurRepository repository ;
    public List <Utilisateur> getAllUsers() {
        return repository.findAll();
    }


    public Utilisateur finduserById(String id) {
        Optional<Utilisateur> utOptional = repository.findById(id);
        if (utOptional.isEmpty()) {
            return null;
        } else {
            return utOptional.get();
        }
    }




    public Utilisateur addUser(Utilisateur user) {
        return repository.save(user);
    }


    public void deleteUser(String id) {
        repository.deleteById(id);

    }


    public Utilisateur updateUser(Utilisateur user) {
        Optional<Utilisateur> utOptional = repository.findById(user.getId());
        if (utOptional.isEmpty()) {
            return null;
        } else {
            return repository.save(user);
        }
    }
        public String save(Utilisateur user){
            return repository.save(user).getId();
        }

    }
