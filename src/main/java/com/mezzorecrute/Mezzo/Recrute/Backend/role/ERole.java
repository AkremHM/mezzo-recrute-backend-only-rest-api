package com.mezzorecrute.Mezzo.Recrute.Backend.role;

public enum ERole {
    ROLE_EMPLOYEE,
    ROLE_RC,
    ROLE_RECRUTEUR,
    ROLE_RD,
    ROLE_ADMIN,
    ROLE_CANDIDAT
}
